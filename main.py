from flask import Flask, \
                render_template, \
                jsonify, \
                request
import json, os

app = Flask(__name__)
app.config['UPLOAD_FOLDER_SLIDE'] = './uploads/slides'
app.config['UPLOAD_FOLDER_GIF'] = './uploads/animations'



@app.route("/")
def index():
    return render_template('index.html')


## apis


### gif CRUD
@app.route("/list")
def api_list():
    return jsonify(['gif_path1' ,'git_path2', 'git_path3'])

@app.route("/upload_gif", methods=['GET' ,'POST'])
def api_upload():

    if request.method =='POST':
        file = request.files['file']
        if file:
            filename = file.filename
            file.save(os.path.join(app.config['UPLOAD_FOLDER_GIF'],filename))


    return jsonify({'info': 'upload_gif'})

@app.route("/delete")
def api_delete():
    return jsonify({'info': 'delete'})


### ppt 
@app.route("/upload_slide", methods = ['GET' , 'POST'])
def api_upload_slide():
    if request.method =='POST':
        file = request.files['file']
        if file:
            filename = file.filename
            file.save(os.path.join(app.config['UPLOAD_FOLDER_SLIDE'],filename))

    return jsonify({'info': 'upload_slide'})


if __name__=='__main__':
    app.run(debug=True)